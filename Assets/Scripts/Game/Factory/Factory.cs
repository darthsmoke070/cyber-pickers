using UnityEngine;

namespace Game {

    public class Factory : MonoBehaviour, IInteractable {

        [SerializeField]
        private Transform _comeCloserTransform;
        public Transform ComeCloserTransform => _comeCloserTransform;

        [SerializeField]
        private Transform _storageItemPos;

        [SerializeField]
        private AnimationCurve _pickDownAnimationCurve;

        [SerializeField]
        private float _pickDownAnimationTime;

        [SerializeField]
        private Sprite _uISprite;
        public Sprite UISprite => _uISprite;

        private Utils.TransformAnimation _pickDownAnimation;

        public void Interact(Picker picker) {
            var storageItem = picker.PickUpLogic.PickDown();
            if (storageItem == null) {
                return;
            }
                
            System.Action onPickDownAnimationComplete = () => {
                storageItem.Disable(false);
            };

            _pickDownAnimation = Utils.TransformAnimation.ProceedAnimation(_pickDownAnimation, this, storageItem.transform, _storageItemPos, _pickDownAnimationCurve, _pickDownAnimationTime, onPickDownAnimationComplete);
        }
    }
}