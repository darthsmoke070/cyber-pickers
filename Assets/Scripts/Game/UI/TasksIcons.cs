using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game.UI {

    [CreateAssetMenu(menuName = "Tasks icons")]
    public class TasksIcons : ScriptableObject {

        [SerializeField]
        private Sprite _moveSprite;

        [SerializeField]
        private Sprite _interactWithFactorySprite;

        [SerializeField]
        private Sprite _interactWithStorageSprite;

        [SerializeField]
        private Sprite _parkingSprite;

        public Sprite GetSprite(PickerTask task) {
            Sprite result = null;
            if (task is MoveTask moveTask) {
                result = _moveSprite;
                if (moveTask is InteractTask interactTask) {
                    switch (interactTask.Interactable) {
                        case Factory factory:
                            result = _interactWithFactorySprite;
                            break;
                        case Storage storage:
                            result = _interactWithStorageSprite;
                            break;
                        case PickerParking pickerParking:
                            result = _parkingSprite;
                            break;
                    }

                }
            }
            return result;
        }
    }
}
