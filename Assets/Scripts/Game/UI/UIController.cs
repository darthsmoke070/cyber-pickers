using UnityEngine;
using UnityEngine.UI;

namespace Game.UI {

    // TODO Add GameManager?
    public class UIController : MonoBehaviour {

        [SerializeField]
        private GameplayData _gameplayData;

        [Header("Layout elements")]
        [SerializeField]
        private Button _alarmButton;

        [SerializeField]
        private Button _freeAllButton;

        [SerializeField]
        private TaskView _taskView;

        private void Awake() {
            _alarmButton.onClick.AddListener(_gameplayData.onAlarm.Invoke);
            _freeAllButton.onClick.AddListener(_gameplayData.onFreePickers.Invoke);
            _taskView.gameObject.SetActive(false);
        }

        private void Update() {
            UpdateTaskView();
        }

        private void UpdateTaskView() {
            _taskView.SetData(_gameplayData.SelectedPicker == null ? null : _gameplayData.SelectedPicker.AI.currentTask);
        }
    }
}
