using UnityEngine;
using UnityEngine.UI;

namespace Game.UI {
    
    public class TaskView : MonoBehaviour {

        [SerializeField]
        private Image _image;

        [SerializeField]
        private TasksIcons _icons;

        private PickerTask _currentTask;

        public void SetData(PickerTask task) {
            if (task == null) {
                gameObject.SetActive(false);
                _currentTask = null;
            }
            if (_currentTask == task) {
                return;
            }
            gameObject.SetActive(true);
            _image.sprite = _icons.GetSprite(task);
        }
    }
}
