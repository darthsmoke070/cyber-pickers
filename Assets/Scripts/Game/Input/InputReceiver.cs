using UnityEngine;
using UnityEngine.InputSystem;
using System;

namespace Game.Input {

    public class InputReceiver : MonoBehaviour {

        [SerializeField]
        private GameplayData _gameplayData;

        [SerializeField]
        private Camera _gameplayCamera;

        [SerializeField]
        private LayerMask _taskableLayerMask;

        [SerializeField]
        private LayerMask _pickersLayerMask;

        [SerializeField]
        private float _raycastDistance = 10f;

        private gen.Input _input;

        private void Awake() {
            _input = new gen.Input();
            _input.Gameplay.Select.performed += OnSelectPerformed;
            _input.Gameplay.SetTask.performed += OnSetTaskPerformed;
        }

        private void OnEnable() {
            _input.Enable();
        }
        
        private void OnDisable() {
            _input.Disable();
        }

        private void OnSelectPerformed(InputAction.CallbackContext callbackContext) {
            var hasSelected = DoPointerClickAction(raycastHit => {
                var picker = raycastHit.collider.GetComponent<Picker>();
                _gameplayData.SelectedPicker = picker;
            }, _pickersLayerMask);
            if (!hasSelected) {
                _gameplayData.SelectedPicker = null;
            }
        }

        private void OnSetTaskPerformed(InputAction.CallbackContext callbackContext) {
            DoPointerClickAction(raycastHit => {
                var interactable = raycastHit.collider.GetComponent<IInteractable>();
                if (interactable != null) {
                    _gameplayData.interactable = interactable;
                    _gameplayData.onInteractBySelectedPickerWithInteractable.Invoke();
                } else {
                    _gameplayData.movePos = raycastHit.point;
                    _gameplayData.onMoveToPositionEvent.Invoke();
                }
            }, _taskableLayerMask);
        }

        private bool DoPointerClickAction(Action<RaycastHit> action, LayerMask raycastLayer) {
            var pointerPosition = _input.Gameplay.PointerPosition.ReadValue<Vector2>();
            var screenToViewportPoint = _gameplayCamera.ScreenToViewportPoint(pointerPosition);
            if (screenToViewportPoint.x >= 0f && screenToViewportPoint.x <= 1f &&
                screenToViewportPoint.y >= 0f && screenToViewportPoint.y <= 1f) {

                var ray = _gameplayCamera.ScreenPointToRay(pointerPosition);
                if (Physics.Raycast(ray, out var hit, _raycastDistance, raycastLayer)) {
                    action.Invoke(hit);
                    return true;
                }
            }
            return false;
        }
    }
}