using UnityEngine;

namespace Game {

    public class PickerAI : MonoBehaviour {

        private Picker _picker;
        public PickerTask currentTask { get; private set; }

        public void Init(Picker picker) {
            _picker = picker;
        }

        public void PerformTask(PickerTask task) {
            if (currentTask != null) {
                currentTask.ForceStopTask(_picker);
            }
            currentTask = task;
            task.Perform(_picker);
        }

        public void OnPickerCompleteDestination() {
            if (currentTask == null) {
                return;
            }
            currentTask.OnPickerCompletePath(_picker);
            currentTask = null;
        }
    }
}
