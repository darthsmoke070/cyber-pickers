using UnityEngine;

namespace Game {

    [CreateAssetMenu(menuName = "Interact task")]
    public class InteractTask : MoveTask {

        [SerializeField]
        private IInteractable _interactable;
        public IInteractable Interactable => _interactable;

        public void Init(IInteractable interactWith) {
            Init(interactWith.ComeCloserTransform.position);
            _interactable = interactWith;
        }

        public override void OnPickerCompletePath(Picker picker) {
            picker.ChangeRotWhileStanding(_interactable.ComeCloserTransform.rotation);
            _interactable.Interact(picker);
            base.OnPickerCompletePath(picker);
        }
    }
}