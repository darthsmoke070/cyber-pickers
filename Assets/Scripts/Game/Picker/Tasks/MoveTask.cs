using UnityEngine;

namespace Game {

    [CreateAssetMenu(menuName = "Move To Task")]
    public class MoveTask : PickerTask {
        
        [SerializeField]
        private Vector3 _position;

        public void Init(Vector3 position) {
            _position = position;
        }

        public override void Perform(Picker picker) {
            picker.MoveTo(_position);
        }

        public override void ForceStopTask(Picker picker) {
        }

        public override void OnPickerCompletePath(Picker picker) {
        }
    }
}
