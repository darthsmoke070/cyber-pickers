using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game {

    public abstract class PickerTask : ScriptableObject {

        public abstract void Perform(Picker picker);
        public abstract void ForceStopTask(Picker picker);
        public abstract void OnPickerCompletePath(Picker picker);
    }
}
