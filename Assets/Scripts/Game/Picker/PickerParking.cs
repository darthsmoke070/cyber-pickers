using UnityEngine;

namespace Game {

    public class PickerParking : MonoBehaviour, IInteractable {

        [SerializeField]
        private Transform _root;

        [SerializeField]
        private Transform _lockPosTransform;

        [SerializeField]
        private Transform _unlockPosTransform;

        [SerializeField]
        private AnimationCurve _lockCurve;

        [SerializeField]
        private AnimationCurve _unlockCurve;

        [SerializeField]
        private float _lockTime;

        [SerializeField]
        private float _unlockTime;

        private Utils.TransformAnimation _rootAnimation;

        public Transform ComeCloserTransform => transform;

        public Sprite UISprite => null;

        public void Interact(Picker picker) {
            LockParking();
        }

        public void LockParking() {
            //_root.enabled = true;
            _rootAnimation = Utils.TransformAnimation.ProceedAnimation(_rootAnimation, this, _root, _lockPosTransform, _lockCurve, _lockTime, null);
        }

        public void UnlockParking() {
            _rootAnimation = Utils.TransformAnimation.ProceedAnimation(_rootAnimation, this, _root, _unlockPosTransform, _unlockCurve, _unlockTime, () => { /*_root.enabled = false; */});
        }
    }
}
