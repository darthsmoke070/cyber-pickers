using UnityEngine;
using UnityEngine.AI;

namespace Game {
 
    public class Picker : MonoBehaviour {

        private NavMeshAgent _agent;
        public Vector3 AgentVelocity => _agent.velocity;
        public float AgentMaxSpeed => _agent.speed;

        [SerializeField]
        private PickerVisual _visual;

        [SerializeField]
        private PickUpLogic _pickUpLogic;
        public PickUpLogic PickUpLogic => _pickUpLogic;

        [SerializeField]
        private PickerAI _aI;
        public PickerAI AI => _aI;

        [SerializeField]
        private Transform _helper;

        [SerializeField]
        private float _rotationSpeed = 10f;

        private bool _prevIsAgentCompleteDestination;
        private Quaternion? _rotateToWhileStanding;

        public void Init(NavMeshAgent agent) {
            // use when to animate picker
            //_agent.updateRotation = false;
            _prevIsAgentCompleteDestination = true;
            _agent = agent;
            _visual.Init(this);
            _aI.Init(this);
            Deselect();
        }

        private void Update() {
            var isAgentMove = _agent.pathPending || _agent.hasPath;
            var isAgentCompleteDestinaton = !isAgentMove && (_agent.destination - transform.position).sqrMagnitude <= _agent.stoppingDistance * _agent.stoppingDistance;
            if (isAgentCompleteDestinaton && !_prevIsAgentCompleteDestination) {
                _prevIsAgentCompleteDestination = true;
                _aI.OnPickerCompleteDestination();
            }
            _prevIsAgentCompleteDestination = isAgentCompleteDestinaton;
            
            transform.position = _agent.transform.position;
            var dt = Time.deltaTime;
            if (!isAgentMove && _rotateToWhileStanding.HasValue) {
                var rot = Quaternion.Lerp(transform.rotation, _rotateToWhileStanding.Value, dt * _rotationSpeed);
                transform.rotation = rot;
                _agent.transform.rotation = rot;
                var rotatedVector = _rotateToWhileStanding.Value * Vector3.forward;
                var dotProduct = Vector3.Dot(transform.forward, rotatedVector);
                if (dotProduct >= 0.9999f) {
                    _rotateToWhileStanding = null;
                }
            } else {
                transform.rotation = Quaternion.Lerp(transform.rotation, _agent.transform.rotation, dt * _rotationSpeed);
            }
        }

        public void MoveTo(Vector3 point) {
            _prevIsAgentCompleteDestination = false;
            _agent.destination = point;
        }

        // TODO rotate it with Lelp somehow
        public void ChangeRotWhileStanding(Quaternion rot) {
            _rotateToWhileStanding = rot;
        }

        public void Select() {
            _visual.EnableSelectFX();
        }

        public void Deselect() {
            _visual.DisableSelectFX();
        }
    }
}