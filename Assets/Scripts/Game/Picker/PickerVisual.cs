using UnityEngine;

namespace Game {
 
    public class PickerVisual : MonoBehaviour {

        [SerializeField]
        private Animator _animator;

        [SerializeField]
        private GameObject _selector;

        private const string PARAM_SPEED = "Speed";

        private Picker _picker;

        public void Init(Picker picker) {
            _picker = picker;
        }

        private void Update() {
            // to do Find a way to animate without using AgentVelocity.magnitude(it s too hard for update)
            var speed = _picker.AgentVelocity.magnitude;
            _animator.SetFloat(PARAM_SPEED, speed / _picker.AgentMaxSpeed);
        }

        public void EnableSelectFX() {
            _selector.SetActive(true);
        }

        public void DisableSelectFX() {
            _selector.SetActive(false);
        }
    }
}