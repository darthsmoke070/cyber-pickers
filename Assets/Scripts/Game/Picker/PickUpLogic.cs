using Utils;
using UnityEngine;

namespace Game {
 
    public class PickUpLogic : MonoBehaviour {

        [SerializeField]
        private Transform _pickUpPlace;

        [SerializeField]
        private AnimationCurve _pickUpAnimationCurve;

        [SerializeField]
        private float _animationTime = 1f;
        public float AnimationTime => _animationTime;
        
        private StorageItem _storageItem;
        public StorageItem StorageItem => _storageItem;

        private TransformAnimation _pickUpTransformAnimation;

        public void PickUp(StorageItem storageItem) {
            _storageItem = storageItem;
            _pickUpTransformAnimation = TransformAnimation.ProceedAnimation(_pickUpTransformAnimation, this, storageItem.transform, _pickUpPlace, _pickUpAnimationCurve, _animationTime, null);
        }

        private void Update() {
            if (_storageItem != null && _pickUpTransformAnimation != null && _pickUpTransformAnimation.IsAnimationComplete) {
                _storageItem.transform.position = _pickUpPlace.transform.position;
                _storageItem.transform.rotation = _pickUpPlace.transform.rotation;
            }
        }

        public StorageItem PickDown() {
            var returnResult = _storageItem;
            _storageItem = null;
            return returnResult;
        }

        public void HideStorageItem() {
            if (_storageItem == null) {
                return;
            }
            var storageItem = PickDown();
            storageItem.Disable(false);
        }
    }
}