using UnityEngine;

namespace Game {
 
    public class PickerSpawner : MonoBehaviour {

        [SerializeField]
        private Picker _pickerPrefab;

        [SerializeField]
        private PickerParking _parking;

        [SerializeField]
        private UnityEngine.AI.NavMeshAgent _agentPrefab;

        private Picker _pickerInstance;


        public Sprite UISprite => throw new System.NotImplementedException();

        public void Spawn() {
            _pickerInstance = Instantiate(_pickerPrefab, transform.position, transform.rotation);
            var agentInstance = Instantiate(_agentPrefab, transform.position, transform.rotation);
            _pickerInstance.Init(agentInstance);
            _parking.UnlockParking();
        }

        public void AskPickerToGoInParking() {
            var interactTask = ScriptableObject.CreateInstance<InteractTask>();
            interactTask.Init(interactWith: _parking);
            _pickerInstance.AI.PerformTask(interactTask);
        }

        public void UnlockParking() {
            _parking.UnlockParking();
        }
    }
}