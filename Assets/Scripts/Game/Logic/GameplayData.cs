using UnityEngine;
using UnityEngine.Events;

namespace Game {

    // Class reqired for Gameplay Logic communication with UI/InputSystem/etc
    [CreateAssetMenu(menuName = "Gameplay Data")]
    public class GameplayData : ScriptableObject {

        public UnityEvent onFreePickers;
        public UnityEvent onAlarm;

        public UnityEvent onSelectPicker;
        public UnityEvent onDeselectPicker;
        private Picker _selectedPicker;
        public Picker SelectedPicker {
            get {
                return _selectedPicker;
            }
            set {
                if (_selectedPicker != null) {
                    _selectedPicker.Deselect();
                    onDeselectPicker.Invoke();
                }
                _selectedPicker = value;
                if (value != null) {
                    _selectedPicker.Select();
                    onSelectPicker.Invoke();
                }
            }
        }

        public IInteractable interactable;
        public UnityEvent onInteractBySelectedPickerWithInteractable;

        public Vector3 movePos;
        public UnityEvent onMoveToPositionEvent;
    }
}