using UnityEngine;

namespace Game {

    public class GameplayLogic : MonoBehaviour {

        [SerializeField]
        private GameplayData _gameplayData;

        [SerializeField]
        private PickerSpawner[] _spawners = new PickerSpawner[2];

        [SerializeField]
        private Storage[] _storages = new Storage[2];

        private bool _canGiveTaskToPickers = true;

        private void Awake() {
            InitGameplayData();

            for (var i = 0; i < _spawners.Length; i++) {
                _spawners[i].Spawn();
            }
            for (var i = 0; i < _storages.Length; i++) {
                _storages[i].Init();
            }
        }

        private void InitGameplayData() {
            _gameplayData.onMoveToPositionEvent.AddListener(() => {
                MoveSelectedPickerToPosition(_gameplayData.movePos);
            });
            _gameplayData.onInteractBySelectedPickerWithInteractable.AddListener(() => {
                AskSelectedPickerToInteract(_gameplayData.interactable);
            });
            _gameplayData.onFreePickers.AddListener(FreeAllPickers);
            _gameplayData.onAlarm.AddListener(LockUpAllPickersInParking);
        }

        public void MoveSelectedPickerToPosition(Vector3 pos) {
            if (!_canGiveTaskToPickers) {
                return;
            }

            if (_gameplayData.SelectedPicker == null) {
                return;
            }

            var moveTask = ScriptableObject.CreateInstance<MoveTask>();
            moveTask.Init(pos);
            _gameplayData.SelectedPicker.AI.PerformTask(moveTask);
        }

        public void AskSelectedPickerToInteract(IInteractable interactable) {
            if (!_canGiveTaskToPickers) {
                return;
            }

            if (_gameplayData.SelectedPicker == null) {
                return;
            }

            var interactTask = ScriptableObject.CreateInstance<InteractTask>();
            interactTask.Init(interactable);
            _gameplayData.SelectedPicker.AI.PerformTask(interactTask);
        }

        public void LockUpAllPickersInParking() {
            for (var i = 0; i < _spawners.Length; i++) {
                _spawners[i].AskPickerToGoInParking();
            }
            _canGiveTaskToPickers = false;
        }

        public void FreeAllPickers() {
            for (var i = 0; i < _spawners.Length; i++) {
                _spawners[i].UnlockParking();
            }
            _canGiveTaskToPickers = true;
        }
    }
}