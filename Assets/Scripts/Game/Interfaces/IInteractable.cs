using UnityEngine;

namespace Game {
 
    public interface IInteractable {

        public Transform ComeCloserTransform { get; }
        public void Interact(Picker picker);
    }
}