using System.Collections.Generic;
using UnityEngine;

namespace Game {
 
    public class Storage : MonoBehaviour, IInteractable {

        [SerializeField]
        private Transform _storageItemSpawnTransform;

        [SerializeField]
        private Transform _comeCloserTransform;
        public Transform ComeCloserTransform { get => _comeCloserTransform; }
        
        [SerializeField]
        private StorageItem _storageItemPrefab;

        [SerializeField]
        private Sprite _uISprite;
        public Sprite UISprite => _uISprite;

        private Utils.GameObjectPool _storageItemPool;

        // Calculated from Task, can automate this calculation later. (2 for picker + 1 for showing) per one Storage
        private const int STORAGE_POOL_SIZE = 3;
        private StorageItem _currentItem;


        public void Init() {
            _storageItemPool = new Utils.GameObjectPool(STORAGE_POOL_SIZE, _storageItemPrefab, _storageItemSpawnTransform);
            EnableNextItem(immediately: true);
        }

        private void EnableNextItem(bool immediately) {
            _currentItem = _storageItemPool.GetFreeItem() as StorageItem;
            _currentItem.transform.position = _storageItemSpawnTransform.position;
            _currentItem.transform.rotation = _storageItemSpawnTransform.rotation;
            _currentItem.Enable(immediately);
        }

        public void Interact(Picker picker) {
            picker.PickUpLogic.HideStorageItem();
            picker.PickUpLogic.PickUp(_currentItem);
            EnableNextItem(immediately: false);
        }
    }
}