using System;
using System.Collections;
using UnityEngine;

namespace Utils {

    public class TransformAnimation {

        private Transform _from;
        private Transform _to;
        private AnimationCurve _animationCurve;
        private float _animationTime;
        private MonoBehaviour _coroutineRoot;
        private Coroutine _animationCoroutine;

        public bool IsAnimationComplete { get; private set; }
        private Action _onAnimationComplete;

        public TransformAnimation(MonoBehaviour coroutineRoot, Transform from, Transform to, AnimationCurve animationCurve, float animationTime, Action onAnimationComplete) {
            SetAnimationParams(from, to, animationCurve, animationTime, onAnimationComplete);
            _coroutineRoot = coroutineRoot;
            _animationCoroutine = null;
            IsAnimationComplete = false;
        }

        public void SetAnimationParams(Transform from, Transform to, AnimationCurve animationCurve, float animationTime, Action onAnimationComplete) {
            _from = from;
            _to = to;
            _animationCurve = animationCurve;
            _animationTime = animationTime;
            _onAnimationComplete = onAnimationComplete;
        }

        public void StartAnimation() {
            if (_animationCoroutine != null) {
                _coroutineRoot.StopCoroutine(_animationCoroutine);
            }
            IsAnimationComplete = false;
            _animationCoroutine = _coroutineRoot.StartCoroutine(AnimationCoroutine());
        }

        private IEnumerator AnimationCoroutine() {
            var currentAnimationTime = 0f;
            var startPos = _from.position;
            var startRot = _from.rotation;
            while (currentAnimationTime < _animationTime) {
                var evaluatedTime = _animationCurve.Evaluate(currentAnimationTime / _animationTime);
                _from.position = Vector3.Lerp(startPos, _to.position, evaluatedTime);
                _from.rotation = Quaternion.Lerp(startRot, _to.rotation, evaluatedTime);
                currentAnimationTime += Time.deltaTime;
                yield return null;
            }
            IsAnimationComplete = true;
            _onAnimationComplete?.Invoke();
        }

        public static TransformAnimation ProceedAnimation(TransformAnimation animation, MonoBehaviour coroutineRoot, Transform from, Transform to, AnimationCurve animationCurve, float animationTime, Action onAnimationComplete) {
            if (animation == null) {
                animation = new TransformAnimation(coroutineRoot, from, to, animationCurve, animationTime, onAnimationComplete);
            } else {
                animation.SetAnimationParams(from, to, animationCurve, animationTime, onAnimationComplete);
            }
            animation.StartAnimation();
            return animation;
        }
    }
}