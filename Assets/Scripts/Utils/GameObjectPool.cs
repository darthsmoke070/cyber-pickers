using System.Linq;
using UnityEngine;

namespace Utils {

    // to do selecting by Component type, not by GameObject?
    public class GameObjectPool {

        private PoolableMonoBehavior[] _pool;

        public GameObjectPool(int size, PoolableMonoBehavior prefab, Transform root) {
            _pool = new PoolableMonoBehavior[size];
            for (var i = 0; i < size; i++) {
                var newPooledObject = Object.Instantiate(prefab, root.position, root.rotation, root);
                newPooledObject.Disable(true);
                _pool[i] = newPooledObject;
            }
        }

        public PoolableMonoBehavior GetFreeItem() {
            var freeItem = _pool.FirstOrDefault(item => !item.IsEnabled);
            if (freeItem == null) {
                Debug.LogError("There is no free items left");
            }
            return freeItem;
        }
    }
}
